document.addEventListener('DOMContentLoaded', function(event) {
    var map = L.map('map').setView([0, 0], 2);

    osm = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
    });

    osm.addTo(map)

        var developers = new L.KML("developers.kml", {async: true});
        developers.on("loaded", function(e) { map.fitBounds(e.target.getBounds()); });
        map.addLayer(developers);

    baseMaps = { 'OpenStreetMap': osm }
    overlayMaps = { 'kernel.org Developers': developers }

    L.control.layers(baseMaps, overlayMaps).addTo(map);
});
